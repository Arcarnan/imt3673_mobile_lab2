# lab 2 #

Done by: Nataniel Gåsøy


### Objective: Simple RSS reader ###

Create an application that allows the user to read content from any RSS feed. The app will consist of 3 activities: one with the list of items (ListView, for selecting content), one for article content display (for reading content), and User Preferences (for user to specify the preferences).

