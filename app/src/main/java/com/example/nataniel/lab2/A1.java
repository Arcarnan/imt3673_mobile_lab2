package com.example.nataniel.lab2;

//  much of the code from this activity is made by Johan Jurrius, https://www.youtube.com/watch?v=Lnan_DJU7DI

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class A1 extends AppCompatActivity
{

    ListView listviewRSS;
    ArrayList <String> titles;
    ArrayList <String> links;

    public Button mSettingsButton;
    public static final String ARTICLE_URL = "URL";
    public SwipeRefreshLayout mSwipeLayout;
    public TextView mFeedTitleTextView;

    private Handler mRepeatHandler;
    private Runnable timedTask;

    public String mFeedTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a1);

        mFeedTitleTextView = (TextView) findViewById(R.id.feedTitle);
        listviewRSS = findViewById(R.id.listViewA1RSS);
        titles = new ArrayList<String>();
        links = new ArrayList<String>();
        mSwipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSettingsButton = findViewById(R.id.settingsButton);
        mRepeatHandler = new Handler();


        //  go to new viewer when clicking an item in the listView
        listviewRSS.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                //get the link of the position clicked (i.e. of the title pressed)
                Uri uri = Uri.parse(links.get(position));

                //----------------------------EDIT for opening the article in browser--------------------------------
                //Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                //---------------------------------------------------------------------------------------------------


                //----------------------------EDIT for opening the article in the app--------------------------------
                Intent intent = new Intent (A1.this, A2.class);
                String message = uri.toString();
                intent.putExtra(ARTICLE_URL, message);
                startActivity (intent);
                //---------------------------------------------------------------------------------------------------
            }
        });


        //  button for changing activity to settings
        mSettingsButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startActivity(new Intent(A1.this, A3.class));
            }
        });

        //  pull-down refresh to reload the feed
        mSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                titles = new ArrayList<String>();
                links = new ArrayList<String>();
                new ProcessInBackground().execute();
                mSwipeLayout.setRefreshing(false);
            }
        });

        new ProcessInBackground().execute();                //TODO pass inn the progress int here

        //  for updating at given time intervals/ fetch data from the provided RSS feed every chosen time interval
        timedTask = new Runnable()
        {
            @Override
            public void run()
            {
                titles = new ArrayList<String>();
                links = new ArrayList<String>();
                new ProcessInBackground().execute();
                mRepeatHandler.postDelayed(timedTask, getDesiredRefreshRate());
            }
        };
        //mRepeatHandler.post(timedTask);
    }


    //  closes app if back button is clicked from the listView activity (A1, the main activity)
    @Override
    public void onBackPressed()
    {
        new AlertDialog.Builder(this)
                .setTitle("Exit application")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface arg0, int arg1)
                    {
                        //A1.super.onBackPressed();
                        finishAffinity();
                    }
                }).create().show();
    }

    //  opens the web viewer for the provided URL (from RSS feed link)
    public InputStream getInputStream (URL url)
    {
      try
      {
          return url.openConnection().getInputStream();
      }
      catch (IOException e)
      {
          return null;
      }
    }

    //  fetch spinner data for desired number of items from the settings activity (A3), and return appropriate value
    public Integer getDesiredNumberOfItems()
    {
        final SharedPreferences numberOfItemsPreference = PreferenceManager.getDefaultSharedPreferences(this);
        final int numberOfItemsSpinnerPosition = numberOfItemsPreference.getInt("L1", 1);
        int desiredNumber = 0;

        if (numberOfItemsSpinnerPosition == 0)
        {
            desiredNumber = 10;
        }

        else if (numberOfItemsSpinnerPosition == 1)
        {
            desiredNumber = 20;
        }

        else if (numberOfItemsSpinnerPosition == 2)
        {
            desiredNumber = 50;
        }

        else if (numberOfItemsSpinnerPosition == 3)
        {
            desiredNumber = 100;
        }
        return desiredNumber;
    }

    public Integer getDesiredRefreshRate()
    {
        final SharedPreferences numberOfItemsPreference = PreferenceManager.getDefaultSharedPreferences(this);
        final int numberOfItemsSpinnerPosition = numberOfItemsPreference.getInt("L2", 1);
        int desiredNumber = 0;

        if (numberOfItemsSpinnerPosition == 0)
        {
            desiredNumber = 1000 * 60 * 10; //10 minutes
        }

        else if (numberOfItemsSpinnerPosition == 1)
        {
            desiredNumber = 1000 * 60 * 60; //1 hours
        }

        else if (numberOfItemsSpinnerPosition == 2)
        {
            desiredNumber = 1000 * 60 * 60 * 24;    //24 hours
        }
        return desiredNumber;
    }

    //  fetch the RSS feed, and populate the listView object
    public class ProcessInBackground extends AsyncTask <Integer, Integer, Exception>   //type accepted, incrementing progress, what is returned
    {

        ProgressDialog progressDialog = new ProgressDialog(A1.this);

        Exception exception = null;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();

            progressDialog.setMessage("Busy loading RSS feed, please wait...");
            progressDialog.show();
        }

        @Override
        protected Exception doInBackground(Integer... integers)
        {
            try {
                SharedPreferences preference = getSharedPreferences("editTextURL", 0);
                String savedURL = preference.getString("L3", "http://feeds.bbci.co.uk/news/world/europe/rss.xml");


                if (!savedURL.startsWith("http://") && !savedURL.startsWith("https://") && !savedURL.startsWith("ht") &&!savedURL.startsWith("http"))

                {
                    savedURL = "http://" + savedURL;
                }

                //check pattern for a valid URL pattern, and that the URL ends with .xml
                if (Patterns.WEB_URL.matcher(savedURL).matches())
                {

                    URL url = new URL(savedURL);

                    XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(false);   //not support for XML namespaces

                    XmlPullParser xpp = factory.newPullParser();

                    try {
                        xpp.setInput(getInputStream(url), "UTF_8");


                        boolean insideItem = false;     //checking if we are currently inside an information item

                        int eventType = xpp.getEventType();
                        int numberOfItems = 0;
                        int desiredNumberOfItems = getDesiredNumberOfItems();


                        while (eventType != XmlPullParser.END_DOCUMENT)
                        {
                            if (eventType == XmlPullParser.START_TAG && numberOfItems < desiredNumberOfItems)         //TODO in here, you add a new "if else" for all the things you want to add (title, description etc)
                            {
                                //get the title of the RSS feed site
                                if (xpp.getName().equalsIgnoreCase("title"))
                                {
                                    if (!insideItem)
                                    {
                                        mFeedTitle = xpp.nextText();
                                    }
                                }

                                if (xpp.getName().equalsIgnoreCase("item") || xpp.getName().equalsIgnoreCase("entry"))
                                {
                                    insideItem = true;

                                } else if (xpp.getName().equalsIgnoreCase("title"))
                                {
                                    if (insideItem) {
                                        titles.add(xpp.nextText());
                                        numberOfItems++;
                                    } else {
                                        //RSSTitle.add(xpp.nextText());           //TODO get the main title...somehow
                                        //mFeedTitle = RSSTitle.get(0);
                                    }
                                } else if (xpp.getName().equalsIgnoreCase("link"))
                                {
                                    if (insideItem) {
                                        links.add(xpp.nextText());
                                    }
                                }
                            } else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase("item"))
                            {
                                insideItem = false;
                            }

                            eventType = xpp.next();
                        }
                    } catch (XmlPullParserException e)
                    {
                        exception = e;
                    }
                }
                else
                {
                    Toast.makeText(A1.this,
                            "Please enter a valid RSS feed URL",
                            Toast.LENGTH_LONG).show();
                }
            }
            catch (MalformedURLException e)
            {
                exception = e;
            }
            catch (XmlPullParserException e)
            {
                exception = e;
            }
            catch (IOException e)
            {
                exception = e;
            }
            return exception;
        }

        @Override
        protected void onPostExecute(Exception s)
        {
            super.onPostExecute(s);

            //sets the mFeedTitle variable for the feed title display in A1
            mFeedTitleTextView.setText(mFeedTitle);


            ArrayAdapter<String> adapter = new ArrayAdapter<String>(A1.this, android.R.layout.simple_list_item_1, titles);
            listviewRSS.setAdapter(adapter);
            progressDialog.dismiss();

            //if no elements in titles, user is prompted to enter a valid URL
            if (titles.size() == 0)
            {
                Toast.makeText(A1.this,
                        "Please enter a valid RSS feed URL",
                        Toast.LENGTH_LONG).show();
            }
        }
    }
}