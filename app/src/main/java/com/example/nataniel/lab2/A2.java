package com.example.nataniel.lab2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class A2 extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a2);

        Intent intent = getIntent();
        String articleURLFromA1 = intent.getStringExtra(A1.ARTICLE_URL);
        WebView myWebView = (WebView) findViewById(R.id.webViewA2Article);
        myWebView.loadUrl(articleURLFromA1);
    }
}
