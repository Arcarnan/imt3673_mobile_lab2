package com.example.nataniel.lab2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class A3 extends AppCompatActivity
{
    public Button mFetchFeedButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a3);

        // For dropdown menu:
        final Spinner spinnerNumberOfItems = findViewById(R.id.spinnerNumberOfItems);
        final Spinner spinnerRefreshRate = findViewById(R.id.spinnerRefreshRate);

        //  list for dropdown choices
        List<String> numberOfItems = new ArrayList<>();
        numberOfItems.add("10");
        numberOfItems.add("20");
        numberOfItems.add("50");
        numberOfItems.add("100");

        List<String> refreshRate = new ArrayList <>();
        refreshRate.add("10 min");
        refreshRate.add("1 hour");
        refreshRate.add("24 hours");

        //saving choices for number of items
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, numberOfItems);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerNumberOfItems.setAdapter(dataAdapter);

        final SharedPreferences numberOfItemsPreference = PreferenceManager.getDefaultSharedPreferences(this);
        final int L1 = numberOfItemsPreference.getInt("L1", -1);
        if (L1 != -1)
        {
            spinnerNumberOfItems.setSelection(L1);
        }

        //saving choices for refresh rate
        ArrayAdapter<String> dataAdapterRefreshRate = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, refreshRate);
        dataAdapterRefreshRate.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerRefreshRate.setAdapter(dataAdapterRefreshRate);

        final SharedPreferences refreshPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        final int L2 = refreshPreferences.getInt("L2", -1);
        if (L2 != -1)
        {
            spinnerRefreshRate.setSelection(L2);
        }

        mFetchFeedButton = (Button) findViewById(R.id.buttonA3Fetch);
        mFetchFeedButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)         //TODO change the activity change to the actuall RSS fetch
            {
                startActivity(new Intent(A3.this, A1.class));
            }
        });
    }


    // For saving settings:
    @Override
    protected void onPause()
    {
        super.onPause();

        //for saving the inputted URL
        final SharedPreferences URLPreferences = getSharedPreferences("editTextURL", 0);
        final SharedPreferences.Editor URLEditor = URLPreferences.edit();
        final EditText URLText = findViewById(R.id.rssFeedEditText);
        final String URLString = URLText.getText().toString();
        if (Patterns.WEB_URL.matcher(URLString).matches() && URLString.endsWith(".xml"))
        {

            URLEditor.putString("L3", URLString);
            URLEditor.apply();
        }
        else
        {
            Toast.makeText(A3.this,
                    "Please enter a valid RSS feed URL",
                    Toast.LENGTH_LONG).show();
        }

        //for saving the choice of number of items
        final SharedPreferences numberOfItemsPreference = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor numberOfItemsEditor = numberOfItemsPreference.edit();
        final Spinner spinnerNumberOfItems = findViewById(R.id.spinnerNumberOfItems);
        final int L1 = spinnerNumberOfItems.getSelectedItemPosition();
        numberOfItemsEditor.putInt("L1", L1);
        numberOfItemsEditor.apply();

        //for saving the choice of refresh rate
        final SharedPreferences refreshPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor refreshEditor = refreshPreferences.edit();
        final Spinner spinnerRefreshRate = findViewById(R.id.spinnerRefreshRate);
        final int L2 = spinnerRefreshRate.getSelectedItemPosition();
        refreshEditor.putInt("L2", L2);
        refreshEditor.apply();
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        final EditText URLText = findViewById(R.id.rssFeedEditText);
        SharedPreferences preference = getSharedPreferences("editTextURL", 0);
        String savedURL = preference.getString("L3", null);
        URLText.setText(savedURL);
    }
}
